package iceland.matchers

import iceland.utils.*

fun fail(): Nothing { throw AssertionError() }
fun fail(msg: String): Nothing { throw AssertionError(msg) }

fun isExpectationMet(checkThat: ()->Any?) = Expectation(checkThat, propagateResult = { it == null })
fun whichFailDoWeGetFor(checkThat: ()->Any?) = Expectation(checkThat, propagateResult = { it })
fun expectThat(checkThat: ()->Any?) = Expectation(checkThat, propagateResult = { if (it != null) throw AssertionError(it) })

class Expectation<TResult>(val getObtained: () -> Any?, val propagateResult: (String?) -> TResult) {

    infix fun isEqualTo(getExpected: () -> Any?) : TResult? {
        val obtained = getObtained()
        val expected = getExpected()
        return propagateResult(
            if (obtained == expected) null
            else "objects are not equal${System.lineSeparator()}   expected: $expected${System.lineSeparator()}   but got: $obtained")
    }

    infix fun isNotEqualTo(getExpected: () -> Any?) : TResult? {
        val obtained = getObtained()
        val expected = getExpected()
        return propagateResult(
            if (obtained != expected) null
            else "objects are equal, although they should not be:${System.lineSeparator()}   $expected")
    }

    infix fun isSameInstanceAs(getExpected: () -> Any?) : TResult? {
        val obtained = getObtained()
        val expected = getExpected()
        return propagateResult(
            if (obtained === expected) null
            else "objects are not the same instance, although they should be")
    }

    infix fun isNotSameInstanceAs(getExpected: () -> Any?) : TResult? {
        val obtained = getObtained()
        val expected = getExpected()
        return propagateResult(
            if (obtained !== expected) null
            else "objects are the same instance, although they should not be")
    }


    val isTrue: TResult? get() {
        val obtained = getObtained()
        if (obtained !is Boolean)
            return propagateResult("obtained value is not boolean, but is: $obtained")
        return propagateResult(
            if (obtained == true) null
            else "expected true, but got $obtained")
    }

    val isFalse: TResult? get() {
        val obtained = getObtained()
        if (obtained !is Boolean)
            return propagateResult("obtained value is not boolean, but is: $obtained")
        return propagateResult(
            if (obtained == false) null
            else "expected false, but got $obtained")
    }

    val isNull: TResult? get() {
        val obtained = getObtained()
        return propagateResult(
            if (obtained == null) null
            else "expected null, but got: $obtained")
    }

    val isNotNull: TResult? get() {
        val obtained = getObtained()
        return propagateResult(
            if (obtained != null) null
            else "expected non-null object, but got null")
    }

    val isNan: TResult? get() {
        val obtained = getObtained()
        if (!obtained.isFloatingPoint)
            return propagateResult("obtained value is not a floating-point value, but is: $obtained")
        return propagateResult(
            if ((obtained!!.asDouble).isNaN()) null
            else "obtained value is not NaN: $obtained")
    }

    val isNotNan: TResult? get() {
        val obtained = getObtained()
        if (!obtained.isFloatingPoint)
            return propagateResult("obtained value is not a floating-point value, but is: $obtained")
        return propagateResult(
            if (!(obtained!!.asDouble).isNaN()) null
            else "obtained value is NaN, although it should not be")
    }

    val isInfinite: TResult? get() {
        val obtained = getObtained()
        if (!obtained.isFloatingPoint)
            return propagateResult("obtained value is not a floating-point value, but is: $obtained")
        return propagateResult(
            if ((obtained!!.asDouble).isInfinite()) null
            else "obtained value is not infinite: $obtained")
    }

    val isFinite: TResult? get() {
        val obtained = getObtained()
        if (!obtained.isFloatingPoint)
            return propagateResult("obtained value is not a floating-point value, but is: $obtained")
        return propagateResult(
            if ((obtained!!.asDouble).isFinite()) null
            else "obtained value is not finite, although it should be")
    }

    @Suppress("UNCHECKED_CAST")
    infix fun isLessThan(getExpected: () -> Any) : TResult? {
        val obtained = getObtained()
        val expected = getExpected()
        return try {
            propagateResult(
                if ((obtained as Comparable<Any>) < (expected as Comparable<Any>)) null
                else "$obtained is not < $expected")
        } catch (exc: Throwable) {
            propagateResult("comparison between $obtained and $expected not supported")
        }
    }

    @Suppress("UNCHECKED_CAST")
    infix fun isLessThanOrEqualTo(getExpected: () -> Any) : TResult? {
        val obtained = getObtained()
        val expected = getExpected()
        return try {
            propagateResult(
                if ((obtained as Comparable<Any>) <= (expected as Comparable<Any>)) null
                else "$obtained is not <= $expected")
        } catch (exc: Throwable) {
            propagateResult("comparison between $obtained and $expected not supported")
        }
    }

    @Suppress("UNCHECKED_CAST")
    infix fun isGreaterThan(getExpected: () -> Any) : TResult? {
        val obtained = getObtained()
        val expected = getExpected()
        return try {
            propagateResult(
                if ((obtained as Comparable<Any>) > (expected as Comparable<Any>)) null
                else "$obtained is not > $expected")
        } catch (exc: Throwable) {
            propagateResult("comparison between $obtained and $expected not supported")
        }
    }

    @Suppress("UNCHECKED_CAST")
    infix fun isGreaterThanOrEqualTo(getExpected: () -> Any) : TResult? {
        val obtained = getObtained()
        val expected = getExpected()
        return try {
            propagateResult(
                if ((obtained as Comparable<Any>) >= (expected as Comparable<Any>)) null
                else "$obtained is not >= $expected")
        } catch (exc: Throwable) {
            propagateResult("comparison between $obtained and $expected not supported")
        }
    }

    inner class ClosenessExpectation(val getExpected: () -> Any) {
        infix fun than(getEpsilon: () -> Any) : TResult? {
            val rawObtained = getObtained()
            val rawExpected = getExpected()
            val rawEpsilon = getEpsilon()

            return try {
                val obtained = objectSupportingArithmetics(rawObtained as Any)
                val expected = objectSupportingArithmetics(rawExpected)
                val epsilon = objectSupportingArithmetics(rawEpsilon)

                propagateResult(
                    if ((obtained - expected).abs < epsilon.abs) null
                    else "$obtained is not closer to $expected than ${epsilon.abs}")
            } catch (exc: Throwable) {
                propagateResult("epsilon range of $rawEpsilon not supported for $rawObtained and $rawExpected")
            }
        }
    }
    infix fun isCloserTo(getExpected: () -> Any) = ClosenessExpectation(getExpected)

    infix fun throws(isExceptionOk: (exc: Throwable) -> Boolean) : TResult? {
        try {
            getObtained()
        } catch (exc: Throwable) {
            return propagateResult(
                if (isExceptionOk(exc)) null
                else "expectation on exception not met")
        }
        return propagateResult("no exception thrown at all")
    }

    @Suppress("UNCHECKED_CAST")
    infix fun <TElement : Any?> hasElements(getExpected: () -> Sequence<TElement>): TResult? =
        try {
            val firstDifference = (getObtained() as Sequence<TElement>).firstDifferenceTo(getExpected())
            propagateResult(
                when (firstDifference) {
                    is NoDifference -> null
                    is LhsIsLonger -> "obtained is longer than expected"
                    is RhsIsLonger -> "expected is longer than obtained"
                    is Difference<*> -> "first difference of elements found at idx ${firstDifference.idx}, " +
                            "expected: ${firstDifference.rhsValue}, but got ${firstDifference.lhsValue}"
                }
            )
        } catch (exc: Throwable) {
            propagateResult("hasElements not supported for given arguments")
        }


}


