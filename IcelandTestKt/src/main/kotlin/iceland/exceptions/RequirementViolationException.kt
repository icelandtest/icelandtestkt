package iceland.exceptions

open class RequirementViolationException(message: kotlin.String? = null, cause: kotlin.Throwable? = null) : BaseException(message, cause)