package iceland.exceptions

open class OutOfBoundsException(message: kotlin.String? = null, cause: kotlin.Throwable? = null) : BaseException(message, cause)