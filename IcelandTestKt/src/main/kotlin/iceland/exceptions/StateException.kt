package iceland.exceptions

open class StateException(message: kotlin.String? = null, cause: kotlin.Throwable? = null) : BaseException(message, cause)