package iceland.exceptions

open class UnsupportedException(message: kotlin.String? = null, cause: kotlin.Throwable? = null) : BaseException(message, cause)