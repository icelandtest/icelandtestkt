package iceland.exceptions

// sorry for having this strange base-exception... it is needed because Exceptions in JavaScript cannot deal with the constructor of Throwable which accepts a cause
open class BaseException(message: String? = null, cause: Throwable? = null) : Throwable(message, cause)