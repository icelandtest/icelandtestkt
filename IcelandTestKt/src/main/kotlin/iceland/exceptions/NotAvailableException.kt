package iceland.exceptions

open class NotAvailableException(message: kotlin.String? = null, cause: kotlin.Throwable? = null) : BaseException(message, cause)