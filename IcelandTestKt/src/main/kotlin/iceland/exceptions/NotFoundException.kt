package iceland.exceptions

open class NotFoundException(message: kotlin.String? = null, cause: kotlin.Throwable? = null) : BaseException(message, cause)