package iceland.exceptions

open class ArgumentException(message: kotlin.String? = null, cause: kotlin.Throwable? = null) : BaseException(message, cause)