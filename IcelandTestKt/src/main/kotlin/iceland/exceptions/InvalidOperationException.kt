package iceland.exceptions

open class InvalidOperationException(message: kotlin.String? = null, cause: kotlin.Throwable? = null) : BaseException(message, cause)