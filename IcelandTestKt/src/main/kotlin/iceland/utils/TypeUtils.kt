package iceland.utils

import iceland.exceptions.InvalidOperationException

val Any?.isNumeric: Boolean get() = this.isIntegral || this.isFloatingPoint

val Any?.isIntegral: Boolean get() = when(this) {
    is Byte, is UByte,
    is Short, is UShort,
    is Int, is UInt,
    is Long, is ULong -> true

    else -> false
}

val Any?.isFloatingPoint: Boolean get() = when(this) {
    is Float, is Double -> true
    else -> false
}

val Any.asDouble: Double get() = when(this) {
    is Double -> this
    is Float -> this.toDouble()
    is Byte -> this.toDouble()
    is UByte -> this.toDouble()
    is Short -> this.toDouble()
    is UShort -> this.toDouble()
    is Int -> this.toDouble()
    is UInt -> this.toDouble()
    is Long -> this.toDouble()
    is ULong -> this.toDouble()
    else -> throw InvalidOperationException("cannot convert to Double: $this")
}