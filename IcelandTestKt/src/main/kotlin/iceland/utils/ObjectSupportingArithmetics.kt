package iceland.utils

import iceland.exceptions.UnsupportedException
import kotlin.math.abs

fun <TValue> objectSupportingArithmetics(value: TValue) = ObjectSupportingArithmetics(value, arithmeticFunctionsFor(value))

class ObjectSupportingArithmetics<TValue>(val value: TValue, private val arithmeticFunctions: ArithmeticFunctionsFor<TValue>) {
    operator fun unaryPlus() =
        ObjectSupportingArithmetics(arithmeticFunctions.applyUnaryPlus(value), arithmeticFunctions)

    operator fun unaryMinus() =
        ObjectSupportingArithmetics(arithmeticFunctions.applyUnaryMinus(value), arithmeticFunctions)

    val abs get() = ObjectSupportingArithmetics(arithmeticFunctions.applyAbs(value), arithmeticFunctions)

    operator fun plus(rhs: ObjectSupportingArithmetics<TValue>) =
        ObjectSupportingArithmetics(arithmeticFunctions.applyPlus(value, rhs.value), arithmeticFunctions)

    operator fun minus(rhs: ObjectSupportingArithmetics<TValue>) =
        ObjectSupportingArithmetics(arithmeticFunctions.applyMinus(value, rhs.value), arithmeticFunctions)

    operator fun times(rhs: ObjectSupportingArithmetics<TValue>) =
        ObjectSupportingArithmetics(arithmeticFunctions.applyTimes(value, rhs.value), arithmeticFunctions)

    operator fun div(rhs: ObjectSupportingArithmetics<TValue>) =
        ObjectSupportingArithmetics(arithmeticFunctions.applyDiv(value, rhs.value), arithmeticFunctions)

    operator fun rem(rhs: ObjectSupportingArithmetics<TValue>) =
        ObjectSupportingArithmetics(arithmeticFunctions.applyRem(value, rhs.value), arithmeticFunctions)

    operator fun compareTo(rhs: ObjectSupportingArithmetics<TValue>) = arithmeticFunctions.compareValues(value, rhs.value)

    val isPositive get() = arithmeticFunctions.isPositive(value) // zero or greater
    val isGreaterThanZero get() = arithmeticFunctions.isGreaterThanZero(value) // greater than zero
    val isNegative get() = arithmeticFunctions.isNegative(value) // smaller than zero
    val isZero get() = arithmeticFunctions.isZero(value)

    override fun toString(): String = value.toString()
    override fun equals(other: Any?): Boolean = when {
        value == null -> other == null
        other is ObjectSupportingArithmetics<*> -> value == other.value
        else -> value == other
    }

    override fun hashCode(): Int = value?.hashCode() ?: 0
}


class ArithmeticFunctionsFor<TValue>(
    val applyUnaryPlus: (value: TValue) -> TValue,
    val applyUnaryMinus: (value: TValue) -> TValue,
    val applyAbs: (value: TValue) -> TValue,
    val applyPlus: (lhs: TValue, rhs: TValue) -> TValue,
    val applyMinus: (lhs: TValue, rhs: TValue) -> TValue,
    val applyTimes: (lhs: TValue, rhs: TValue) -> TValue,
    val applyDiv: (lhs: TValue, rhs: TValue) -> TValue,
    val applyRem: (lhs: TValue, rhs: TValue) -> TValue,
    val compareValues: (lhs: TValue, rhs: TValue) -> Int,
    val isPositive: (value: TValue) -> Boolean, // 0 or greater...
    val isGreaterThanZero: (value: TValue) -> Boolean, // ... vs. greater than zero
    val isZero: (value: TValue) -> Boolean,
    val isNegative: (value: TValue) -> Boolean)

@Suppress("UNCHECKED_CAST")
fun <TValue> arithmeticFunctionsFor(example: TValue) : ArithmeticFunctionsFor<TValue> = when(example) {
    is Byte -> arithmeticFunctionsForByte as ArithmeticFunctionsFor<TValue>
    is Short -> arithmeticFunctionsForShort as ArithmeticFunctionsFor<TValue>
    is Int -> arithmeticFunctionsForInt as ArithmeticFunctionsFor<TValue>
    is Long -> arithmeticFunctionsForLong as ArithmeticFunctionsFor<TValue>
    is Float -> arithmeticFunctionsForFloat as ArithmeticFunctionsFor<TValue>
    is Double -> arithmeticFunctionsForDouble as ArithmeticFunctionsFor<TValue>
    else -> throw UnsupportedException("there is no ArithmeticFunctions object for $example")
}

val arithmeticFunctionsForByte = ArithmeticFunctionsFor<Byte>(
    applyUnaryPlus = { it },
    applyUnaryMinus = { (-it).toByte() },
    applyAbs = { abs(it.toInt()).toByte() },
    applyPlus = { lhs, rhs -> (lhs + rhs).toByte() },
    applyMinus = { lhs, rhs -> (lhs - rhs).toByte() },
    applyTimes = { lhs, rhs -> (lhs * rhs).toByte() },
    applyDiv = { lhs, rhs -> (lhs / rhs).toByte() },
    applyRem = { lhs, rhs -> (lhs.rem(rhs)).toByte() },
    compareValues = { lhs, rhs -> lhs.compareTo(rhs) },
    isPositive = { it >= 0.toByte() },
    isGreaterThanZero = { it > 0.toByte() },
    isZero = { it == 0.toByte() },
    isNegative = { it < 0.toByte() })

val arithmeticFunctionsForShort = ArithmeticFunctionsFor<Short>(
    applyUnaryPlus = { it },
    applyUnaryMinus = { (-it).toShort() },
    applyAbs = { abs(it.toInt()).toShort() },
    applyPlus = { lhs, rhs -> (lhs + rhs).toShort() },
    applyMinus = { lhs, rhs -> (lhs - rhs).toShort() },
    applyTimes = { lhs, rhs -> (lhs * rhs).toShort() },
    applyDiv = { lhs, rhs -> (lhs / rhs).toShort() },
    applyRem = { lhs, rhs -> (lhs.rem(rhs)).toShort() },
    compareValues = { lhs, rhs -> lhs.compareTo(rhs) },
    isPositive = { it >= 0.toShort() },
    isGreaterThanZero = { it > 0.toShort() },
    isZero = { it == 0.toShort() },
    isNegative = { it < 0.toShort() })

val arithmeticFunctionsForInt = ArithmeticFunctionsFor<Int>(
    applyUnaryPlus = { it },
    applyUnaryMinus = { -it },
    applyAbs = { abs(it) },
    applyPlus = { lhs, rhs -> lhs + rhs },
    applyMinus = { lhs, rhs -> lhs - rhs },
    applyTimes = { lhs, rhs -> lhs * rhs },
    applyDiv = { lhs, rhs -> lhs / rhs },
    applyRem = { lhs, rhs -> lhs.rem(rhs) },
    compareValues = { lhs, rhs -> lhs.compareTo(rhs) },
    isPositive = { it >= 0 },
    isGreaterThanZero = { it > 0 },
    isZero = { it == 0 },
    isNegative = { it < 0 })

val arithmeticFunctionsForLong = ArithmeticFunctionsFor<Long>(
    applyUnaryPlus = { it },
    applyUnaryMinus = { -it },
    applyAbs = { abs(it) },
    applyPlus = { lhs, rhs -> lhs + rhs },
    applyMinus = { lhs, rhs -> lhs - rhs },
    applyTimes = { lhs, rhs -> lhs * rhs },
    applyDiv = { lhs, rhs -> lhs / rhs },
    applyRem = { lhs, rhs -> lhs.rem(rhs) },
    compareValues = { lhs, rhs -> lhs.compareTo(rhs) },
    isPositive = { it >= 0L },
    isGreaterThanZero = { it > 0L },
    isZero = { it == 0L },
    isNegative = { it < 0L })

val arithmeticFunctionsForFloat = ArithmeticFunctionsFor<Float>(
    applyUnaryPlus = { it },
    applyUnaryMinus = { -it },
    applyAbs = { abs(it) },
    applyPlus = { lhs, rhs -> lhs + rhs },
    applyMinus = { lhs, rhs -> lhs - rhs },
    applyTimes = { lhs, rhs -> lhs * rhs },
    applyDiv = { lhs, rhs -> lhs / rhs },
    applyRem = { lhs, rhs -> lhs.rem(rhs) },
    compareValues = { lhs, rhs -> lhs.compareTo(rhs) },
    isPositive = { it >= 0.0f },
    isGreaterThanZero = { it > 0.0f },
    isZero = { it == 0.0f },
    isNegative = { it < 0.0f })

val arithmeticFunctionsForDouble = ArithmeticFunctionsFor<Double>(
    applyUnaryPlus = { it },
    applyUnaryMinus = { -it },
    applyAbs = { abs(it) },
    applyPlus = { lhs, rhs -> lhs + rhs },
    applyMinus = { lhs, rhs -> lhs - rhs },
    applyTimes = { lhs, rhs -> lhs * rhs },
    applyDiv = { lhs, rhs -> lhs / rhs },
    applyRem = { lhs, rhs -> lhs.rem(rhs) },
    compareValues = { lhs, rhs -> lhs.compareTo(rhs) },
    isPositive = { it >= 0.0 },
    isGreaterThanZero = { it > 0.0 },
    isZero = { it == 0.0 },
    isNegative = { it < 0.0 })