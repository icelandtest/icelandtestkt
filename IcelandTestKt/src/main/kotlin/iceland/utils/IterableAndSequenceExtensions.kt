package iceland.utils

//---------------------------------------
sealed class DifferenceBetweenSequences

// sorry for the dummy ctor-argument, but data-classes must have at least one property...
data class NoDifference(private val dummy: Byte = 0) : DifferenceBetweenSequences() {
    override fun toString() = "NoDifference()" // avoid publishing the dummy to a string, it would look strange
}
data class LhsIsLonger(val rhsLength: Int) : DifferenceBetweenSequences()
data class RhsIsLonger(val lhsLength: Int) : DifferenceBetweenSequences()
data class Difference<TElement : Any?>(val idx: Int, val lhsValue: TElement?, val rhsValue: TElement?) : DifferenceBetweenSequences()
//---------------------------------------

fun <TElement : Any?> Sequence<TElement>.firstDifferenceTo(rhs: Sequence<TElement>) : DifferenceBetweenSequences {
    var idx = 0
    val lhsIterator = this.iterator()
    val rhsIterator = rhs.iterator()

    while(lhsIterator.hasNext()) {
        if (!rhsIterator.hasNext())
            return LhsIsLonger(idx)
        val lhsNextElement = lhsIterator.next()
        val rhsNextElement = rhsIterator.next()
        if ((lhsNextElement == null) xor (rhsNextElement == null))
            return Difference(idx, lhsNextElement, rhsNextElement)
        if (lhsNextElement != rhsNextElement)
            return Difference(idx, lhsNextElement, rhsNextElement)
        ++idx
    }
    return if (rhsIterator.hasNext()) RhsIsLonger(idx) else NoDifference()
}

fun <TElement : Any?> Iterable<TElement>.firstDifferenceTo(rhs: Iterable<TElement>) =
    this.asSequence().firstDifferenceTo(rhs.asSequence())
