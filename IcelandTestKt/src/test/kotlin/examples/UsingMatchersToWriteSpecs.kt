package examples

import iceland.exceptions.InvalidOperationException
import org.junit.jupiter.api.Test as spec
import iceland.matchers.*

class UsingMatchersToWriteSpecs {

    // in the following there are small examples how to use the matchers
    // for a detailed specification of the behaviour of the matchers please have
    // a look at their specs which can be found in iceland/matchers/MatchersSpecs.kt in the test-module

    fun doSomething(value: Int) = if (value < 0) throw InvalidOperationException("oops") else value + 10

    @spec
    fun `fail is used to force an unconditional fail of a spec`() {
        try {
            doSomething(-2)
            fail("expected exception not thrown")
        } catch (exc: InvalidOperationException) {
            // we want to end up here :-)
        }
    }

    @spec
    fun `there are three ways to use the matchers`() {
        // the first way is to use expectThat
        // this is the standard for writing specs - it makes the spec fail
        // if an expectation is not met
        expectThat { 1 == 1 }.isTrue

        // the second way is to use isExpectationMet
        // this one simply tells through a boolean return value if an expectation is met
        // and can e.g. be quite useful when using matchers in production code for
        // testing user-input or something like that
        val isItOk = isExpectationMet { 1 == 1 }.isTrue
        if (isItOk == false) // we have to query for true or false explicitly because we obtain a Boolean?
            fail()

        // the third way is to use whichFailDoWeGetFor
        // this one returns a nullable string being null if everything was ok
        // or containing an appropriate error message if an expectation is not met
        val possibleErrorMsg = whichFailDoWeGetFor { 1 == 1 }.isTrue
        if (possibleErrorMsg != null)
            fail("we should not have gotten an error message")
    }

    @spec
    fun `we can write checks for boolean values using isFalse or isTrue`() {
        expectThat { 1 % 2 == 0 }.isFalse
        expectThat { 4 % 2 == 0 }.isTrue
    }

    @spec
    fun `we can write checks for null values using isNull or isNotNull`() {
        expectThat { null }.isNull
        expectThat { "whatever" }.isNotNull
    }

    @spec
    fun `we can write checks for equality using isEqualTo or isNotEqualTo`() {
        expectThat { 1 } isEqualTo { 1 }
        expectThat { 1 } isNotEqualTo { 2 }
    }

    @spec
    fun `we can write checks for closeness for numeric types`() {
        expectThat { 1.0 } isCloserTo { 1.1 } than { 0.5 }
        expectThat { 10 } isCloserTo { 12 } than { 3 }
        expectThat {
            isExpectationMet { 10 } isCloserTo { 12 } than { 2 } // the epsilon itself is excluded!
        }.isFalse
    }

    @spec
    fun `we can write checks for reference-equality using isSameInstanceAs or isNotSameInstanceAs`() {
        val justAString = "whateverXXX".substring(0..6)
        val justAnotherString = "whateverXXX".substring(0..6)

        expectThat { justAString } isSameInstanceAs { justAString }
        expectThat { justAString } isNotSameInstanceAs { justAnotherString }
    }

    @spec
    fun `we can write checks for certain properties of floating-point numbers`() {
        expectThat { 1.0 }.isNotNan
        expectThat { 0.0 / 0.0 }.isNan
        expectThat { 1.0 / 0.0 }.isInfinite
        expectThat { 17.0 / 4.0 }.isFinite
    }

    @spec
    fun `we can write checks for ordering relations if the participants in the checks have comparable types`() {
        expectThat { 1 } isLessThan { 2 }
        expectThat { 2 } isGreaterThan { 1 }
        expectThat { 1 } isLessThanOrEqualTo { 1 }
        expectThat { 1 } isGreaterThanOrEqualTo { 1 }
    }

    @spec
    fun `we can write checks for expected exceptions`() {
        expectThat { doSomething(-1) } throws { it is InvalidOperationException }

        expectThat { doSomething(-1) } throws { // if we want to, we can write more detailed specs as well
            it is InvalidOperationException &&
            it.message == "oops"
        }
    }

    @spec
    fun `we can write checks for sequence-equality`() {
        expectThat { sequenceOf(1, 2, 3) } hasElements { sequenceOf( 1, 2, 3) }
    }

}