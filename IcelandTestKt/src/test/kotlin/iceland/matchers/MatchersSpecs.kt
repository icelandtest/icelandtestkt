package iceland.matchers

import iceland.exceptions.ArgumentException
import iceland.exceptions.InvalidOperationException
import org.junit.jupiter.api.Test as spec

class MatchersSpecs {

    @spec 
    fun `fail forces a spec to fail unconditionally`() {
        var reachedLineThatShouldNotBeReached = false
        try {
            fail()
            @Suppress("UNREACHABLE_CODE")
            reachedLineThatShouldNotBeReached = true // the compiler detects this as unreachable, but just for paranoia reasons (=somebody changes the implementation of fail()) we leave it here
        } catch(exc: AssertionError) {
            // we want to end up here and there is nothing special to check...
        }
        if (reachedLineThatShouldNotBeReached)
            throw AssertionError("a call to fail() did not throw the expected exception")
        try {
            fail("message")
            @Suppress("UNREACHABLE_CODE")
            reachedLineThatShouldNotBeReached = true // the compiler detects this as unreachable, but just for paranoia reasons (=somebody changes the implementation of fail()) we leave it here
        } catch(exc: AssertionError) {
            if (exc.message!! != "message")
                throw AssertionError("expected message is not contained in the AssertionError")
        }
        if (reachedLineThatShouldNotBeReached)
            throw AssertionError("a call to fail(\"message\") did not throw the expected exception")
    }

    @spec
    fun `equality of two objects can be checked`() {
        expectThat { 3 + 4 } isEqualTo { 7 }
        try {
            expectThat { 3 + 4 } isEqualTo { 8 }
            fail("expectation did not fail although it should have")
        } catch (exc: AssertionError) {
            val errorMsg = exc.message
            if (errorMsg != "objects are not equal${System.lineSeparator()}   expected: 8${System.lineSeparator()}   but got: 7")
                fail("wrong error message")
            return
        } catch (exc: Throwable) {
            fail("unexpected exception thrown")
        }
    }

    @spec
    fun `unequality of two objects can be checked`() {
        expectThat { 17 + 4 } isNotEqualTo { 20 }
        try {
            expectThat { 17 + 4 } isNotEqualTo { 21 }
            fail("spec did not fail although it should have")
        } catch (exc: AssertionError) {
            val errorMsg = exc.message
            if (errorMsg != "objects are equal, although they should not be:${System.lineSeparator()}   21")
                fail("wrong error message")
            return
        } catch (exc: Throwable) {
            fail("unexpected exception thrown")
        }
    }

    @spec
    fun `values can be checked for being true`() {
        expectThat { true }.isTrue
        try {
            expectThat { false }.isTrue
            fail("spec did not fail although it should have")
        } catch (exc: AssertionError) {
            expectThat { exc.message } isEqualTo { "expected true, but got false" }
            return
        } catch (exc: Throwable) {
            fail("unexpected exception thrown")
        }
    }

    @spec
    fun `not passing a boolean value to a check for true is a fail`() {
        try {
            expectThat { 666 }.isTrue
            fail("spec did not fail although it should have")
        } catch (exc: AssertionError) {
            expectThat { exc.message } isEqualTo { "obtained value is not boolean, but is: 666" }
            return
        } catch (exc: Throwable) {
            fail("unexpected exception thrown")
        }
    }

    @spec
    fun `values can be checked for being false`() {
        expectThat { false }.isFalse
        try {
            expectThat { true }.isFalse
            fail("spec did not fail although it should have")
        } catch (exc: AssertionError) {
            expectThat { exc.message } isEqualTo { "expected false, but got true" }
            return
        } catch (exc: Throwable) {
            fail("unexpected exception thrown")
        }
    }

    @spec
    fun `not passing a boolean value to a check for false is a fail`() {
        try {
            expectThat { 666 }.isFalse
            fail("spec did not fail although it should have")
        } catch (exc: AssertionError) {
            expectThat { exc.message } isEqualTo { "obtained value is not boolean, but is: 666" }
            return
        } catch (exc: Throwable) {
            fail("unexpected exception thrown")
        }
    }

    @spec
    fun `values can be checked for being null`() {
        expectThat { null }.isNull
        try {
            expectThat { 17 }.isNull
            fail("spec did not fail although it should have")
        } catch (exc: AssertionError) {
            expectThat { exc.message } isEqualTo { "expected null, but got: 17" }
            return
        } catch (exc: Throwable) {
            fail("unexpected exception thrown")
        }
    }

    @spec
    fun `values can be checked for being non-null`() {
        expectThat { 17 }.isNotNull
        try {
            expectThat { null }.isNotNull
            fail("spec did not fail although it should have")
        } catch (exc: AssertionError) {
            expectThat { exc.message } isEqualTo { "expected non-null object, but got null" }
            return
        } catch (exc: Throwable) {
            fail("unexpected exception thrown")
        }
    }

    @spec
    fun `expectThat is used for specs as it fails when an expectation is not met`() {
        expectThat { 3 + 4 } isEqualTo { 7 } // everything is ok...
        try {
            expectThat { 3 + 4 } isEqualTo { 8 }
            fail("expectation did not fail although it should have")
        } catch (exc: AssertionError) {
            // we want to end up here :-)
            return
        } catch (exc: Throwable) {
            fail("unexpected exception thrown")
        }
    }

    @spec
    fun `isExpectationMet is used for asking if an expectation is met as it does not throw`() {
        expectThat {
            isExpectationMet { 17 + 4 } isEqualTo { 21 }
        }.isTrue

        expectThat {
            isExpectationMet { 17 + 4 } isEqualTo { 20 }
        }.isFalse
    }

    @spec
    fun `whichFailDoWeGetFor is used to obtain eventual detailed error descriptions, but it does not throw`() {
        expectThat {
            whichFailDoWeGetFor { 17 + 4 } isEqualTo { 21 }
        }.isNull

        expectThat {
            whichFailDoWeGetFor { 17 + 4 } isEqualTo { 20 }
        } isEqualTo { "objects are not equal${System.lineSeparator()}   expected: 20${System.lineSeparator()}   but got: 21" }
    }

    @spec
    fun `two objects can be checked for reference-identity`() {
        // sorry for the madness below... have to do this in order to keep the compiler from optimizing
        // the string references and making the two objects point to the same instance...
        val obj1 = "OneXXX".substring(0 until 3)
        val obj2 = "OneXXX".substring(0 until 3)

        expectThat { obj1 } isSameInstanceAs { obj1 }

        expectThat {
            whichFailDoWeGetFor { obj1 } isSameInstanceAs { obj2 }
        } isEqualTo { "objects are not the same instance, although they should be" }
    }

    @spec
    fun `two objects can be checked for non-reference-identity`() {
        // sorry for the madness below... have to do this in order to keep the compiler from optimizing
        // the string references and making the two objects point to the same instance...
        val obj1 = "OneXXX".substring(0 until 3)
        val obj2 = "OneXXX".substring(0 until 3)

        expectThat { obj1 } isNotSameInstanceAs  { obj2 }

        expectThat {
            whichFailDoWeGetFor { obj1 } isNotSameInstanceAs { obj1 }
        } isEqualTo { "objects are the same instance, although they should not be" }
    }

    @spec
    fun `floating-point values can be checked for being NaN`() {
        expectThat { Double.NaN }.isNan
        expectThat { Float.NaN }.isNan

        expectThat {
            whichFailDoWeGetFor { 12.0 }.isNan
        } isEqualTo { "obtained value is not NaN: ${12.0}"}

        expectThat {
            whichFailDoWeGetFor { 12.0f }.isNan
        } isEqualTo { "obtained value is not NaN: ${12.0f}"}

        expectThat {
            whichFailDoWeGetFor { 12 }.isNan
        } isEqualTo { "obtained value is not a floating-point value, but is: 12"}
    }

    @spec
    fun `floating-point values can be checked for not being NaN`() {
        expectThat { 21.0 }.isNotNan
        expectThat { 21.0f }.isNotNan

        expectThat {
            whichFailDoWeGetFor { Double.NaN }.isNotNan
        } isEqualTo { "obtained value is NaN, although it should not be"}

        expectThat {
            whichFailDoWeGetFor { Float.NaN }.isNotNan
        } isEqualTo { "obtained value is NaN, although it should not be"}

        expectThat {
            whichFailDoWeGetFor { 21 }.isNan
        } isEqualTo { "obtained value is not a floating-point value, but is: 21"}
    }

    @spec
    fun `floating-point values can be checked for being infinite`() {
        expectThat { Double.POSITIVE_INFINITY }.isInfinite
        expectThat { Double.NEGATIVE_INFINITY }.isInfinite

        expectThat { Float.POSITIVE_INFINITY }.isInfinite
        expectThat { Float.NEGATIVE_INFINITY }.isInfinite

        expectThat {
            whichFailDoWeGetFor { 42.0 }.isInfinite
        } isEqualTo { "obtained value is not infinite: ${42.0}"}

        expectThat {
            whichFailDoWeGetFor { 42.0f }.isInfinite
        } isEqualTo { "obtained value is not infinite: ${42.0}"}

        expectThat {
            whichFailDoWeGetFor { 42 }.isInfinite
        } isEqualTo { "obtained value is not a floating-point value, but is: 42"}
    }

    @spec
    fun `floating-point values can be checked for being finite`() {
        expectThat { 42.0 }.isFinite
        expectThat { 42.0f }.isFinite

        expectThat {
            whichFailDoWeGetFor { Double.POSITIVE_INFINITY }.isFinite
        } isEqualTo { "obtained value is not finite, although it should be"}

        expectThat {
            whichFailDoWeGetFor { Double.NEGATIVE_INFINITY }.isFinite
        } isEqualTo { "obtained value is not finite, although it should be"}

        expectThat {
            whichFailDoWeGetFor { Float.POSITIVE_INFINITY }.isFinite
        } isEqualTo { "obtained value is not finite, although it should be"}

        expectThat {
            whichFailDoWeGetFor { Float.NEGATIVE_INFINITY }.isFinite
        } isEqualTo { "obtained value is not finite, although it should be"}

        expectThat {
            whichFailDoWeGetFor { 42 }.isFinite
        } isEqualTo { "obtained value is not a floating-point value, but is: 42"}
    }

    @spec
    fun `checks for expected exceptions are supported`() {
        expectThat { throw InvalidOperationException() } throws { it is InvalidOperationException }

        expectThat {
            whichFailDoWeGetFor { } throws { it is InvalidOperationException }
        } isEqualTo { "no exception thrown at all"}

        expectThat {
            whichFailDoWeGetFor { throw ArgumentException() } throws { it is InvalidOperationException }
        } isEqualTo { "expectation on exception not met" }
    }

    data class NonComparable(val blubb: Int)

    @spec
    fun `isLessThan is supported for comparable objects`() {
        expectThat { 1.0 } isLessThan { 2.0 }
        expectThat { "abc" } isLessThan { "def" }

        expectThat {
            whichFailDoWeGetFor { 2.0 } isLessThan { 2.0 }
        } isEqualTo { "${2.0} is not < ${2.0}" }

        expectThat {
            whichFailDoWeGetFor { "otto" } isLessThan { 2.0 }
        } isEqualTo { "comparison between otto and ${2.0} not supported" }

        expectThat {
            whichFailDoWeGetFor { NonComparable(1) } isLessThan { NonComparable(2) }
        } isEqualTo { "comparison between ${NonComparable(1)} and ${NonComparable(2)} not supported" }
    }
    @spec
    fun `isLessThanOrEqualTo is supported for comparable objects`() {
        expectThat { 1.0 } isLessThanOrEqualTo { 2.0 }
        expectThat { 1.0 } isLessThanOrEqualTo { 1.0 }
        expectThat { "abc" } isLessThanOrEqualTo { "def" }
        expectThat { "abc" } isLessThanOrEqualTo { "abc" }

        expectThat {
            whichFailDoWeGetFor { 2.0 } isLessThanOrEqualTo { 1.0 }
        } isEqualTo { "${2.0} is not <= ${1.0}" }

        expectThat {
            whichFailDoWeGetFor { "otto" } isLessThanOrEqualTo { 2.0 }
        } isEqualTo { "comparison between otto and ${2.0} not supported" }

        expectThat {
            whichFailDoWeGetFor { NonComparable(1) } isLessThanOrEqualTo { NonComparable(2) }
        } isEqualTo { "comparison between ${NonComparable(1)} and ${NonComparable(2)} not supported" }
    }

    @spec
    fun `isGreaterThan is supported for comparable objects`() {
        expectThat { 2.0 } isGreaterThan { 1.0 }
        expectThat { "def" } isGreaterThan { "abc" }

        expectThat {
            whichFailDoWeGetFor { 2.0 } isGreaterThan { 2.0 }
        } isEqualTo { "${2.0} is not > ${2.0}" }

        expectThat {
            whichFailDoWeGetFor { "otto" } isGreaterThan { 2.0 }
        } isEqualTo { "comparison between otto and ${2.0} not supported" }

        expectThat {
            whichFailDoWeGetFor { NonComparable(1) } isGreaterThan { NonComparable(2) }
        } isEqualTo { "comparison between ${NonComparable(1)} and ${NonComparable(2)} not supported" }
    }

    @spec
    fun `isGreaterThanOrEqualTo is supported for comparable objects`() {
        expectThat { 2.0 } isGreaterThanOrEqualTo { 1.0 }
        expectThat { 2.0 } isGreaterThanOrEqualTo { 2.0 }
        expectThat { "def" } isGreaterThanOrEqualTo { "abc" }
        expectThat { "def" } isGreaterThanOrEqualTo { "def" }

        expectThat {
            whichFailDoWeGetFor { 1.0 } isGreaterThanOrEqualTo { 2.0 }
        } isEqualTo { "${1.0} is not >= ${2.0}" }

        expectThat {
            whichFailDoWeGetFor { "otto" } isGreaterThanOrEqualTo { 2.0 }
        } isEqualTo { "comparison between otto and ${2.0} not supported" }

        expectThat {
            whichFailDoWeGetFor { NonComparable(1) } isGreaterThanOrEqualTo { NonComparable(2) }
        } isEqualTo { "comparison between ${NonComparable(1)} and ${NonComparable(2)} not supported" }
    }

    @spec
    fun `checks for closeness with an epsilon range are supported`() {
        expectThat { 2.0 } isCloserTo { 2.3 } than { 0.5 }
        expectThat { 17 } isCloserTo { 21 } than { 5 }

        expectThat {
            whichFailDoWeGetFor { 2.0 } isCloserTo { 2.3 } than { 0.1 }
        } isEqualTo { "${2.0} is not closer to ${2.3} than ${0.1}" }

        expectThat {
            whichFailDoWeGetFor { 17 } isCloserTo { 21 } than { 4 } // the exact epsilon-match is NOT included, we must be CLOSER!
        } isEqualTo { "17 is not closer to 21 than 4" }

        expectThat {
            whichFailDoWeGetFor { 17 } isCloserTo { 21 } than { "whatever" }
        } isEqualTo { "epsilon range of whatever not supported for 17 and 21" }

        expectThat {
            whichFailDoWeGetFor { null } isCloserTo { 21 } than { 3 }
        } isEqualTo { "epsilon range of 3 not supported for ${null} and 21" }

    }

    @spec
    fun `checks on the equality of two complete sequences are supported`(){
        expectThat { sequenceOf(1, 2) } hasElements { sequenceOf(1, 2) }
        expectThat { sequenceOf<Int>() } hasElements { sequenceOf<Int>() }
        expectThat { mapOf(1 to "one", 2 to "two").asSequence() } hasElements { mapOf(1 to "one", 2 to "two").asSequence() }

        // when generating a sequence from a map and testing against a list of specific values, we have to map the sequence
        // that we obtain from the map to key-value pairs... otherwise we would have to deal with map-internal datastructures :-(
        expectThat { mapOf(1 to "one", 2 to "two").asSequence().map { it.key to it.value } } hasElements
                { sequenceOf(1 to "one", 2 to "two") }

        expectThat {
            whichFailDoWeGetFor { sequenceOf(1, 2) } hasElements { sequenceOf(1, 0) }
        } isEqualTo { "first difference of elements found at idx 1, expected: 0, but got 2" }

        expectThat {
            whichFailDoWeGetFor { sequenceOf(1, 2) } hasElements { sequenceOf(1) }
        } isEqualTo { "obtained is longer than expected" }

        expectThat {
            whichFailDoWeGetFor { sequenceOf(1, 2) } hasElements { sequenceOf(1, 2, 3) }
        } isEqualTo { "expected is longer than obtained" }

        expectThat {
            whichFailDoWeGetFor { sequenceOf(1) } hasElements { sequenceOf<Int>() }
        } isEqualTo { "obtained is longer than expected" }

        expectThat {
            whichFailDoWeGetFor { "whatever" } hasElements { sequenceOf(1, 2) }
        } isEqualTo { "hasElements not supported for given arguments" }
    }

}