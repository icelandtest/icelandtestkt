package iceland.utils

import iceland.matchers.*
import org.junit.jupiter.api.Test as spec

class IterableAndSequenceExtensionsSpecs {
    
    @spec
    fun theFirstDifferenceBetweenTwoIterablesCanBeObtained() {
        expectThat { listOf(1, 2, 3).firstDifferenceTo(listOf(1, 2, 3)) } isEqualTo { NoDifference() }
        expectThat { listOf(1, null, 3).firstDifferenceTo(listOf(1, null, 3)) } isEqualTo { NoDifference() }

        expectThat { listOf(1, 0, 3).firstDifferenceTo(listOf(1, 2, 3)) } isEqualTo { Difference(1, 0, 2) }
        expectThat { listOf(1, 2, 1).firstDifferenceTo(listOf(1, 2, 3)) } isEqualTo { Difference(2, 1, 3) }

        expectThat { listOf(1, 2, 3).firstDifferenceTo(listOf(1, 2)) } isEqualTo { LhsIsLonger(2) }
        expectThat { listOf(1, 2).firstDifferenceTo(listOf(1, 2, 3)) } isEqualTo { RhsIsLonger(2) }

        expectThat { listOf(1, 2, 3).firstDifferenceTo(listOf()) } isEqualTo { LhsIsLonger(0) }
        expectThat { listOf<Int>().firstDifferenceTo(listOf(1, 2, 3)) } isEqualTo { RhsIsLonger(0) }
    }

    @spec
    fun theFirstDifferenceBetweenTwoSequencesCanBeObtained() {
        expectThat { sequenceOf(1, 2, 3).firstDifferenceTo(sequenceOf(1, 2, 3)) } isEqualTo { NoDifference() }
        expectThat { sequenceOf(1, null, 3).firstDifferenceTo(sequenceOf(1, null, 3)) } isEqualTo { NoDifference() }

        expectThat { sequenceOf(1, 0, 3).firstDifferenceTo(sequenceOf(1, 2, 3)) } isEqualTo { Difference(1, 0, 2) }
        expectThat { sequenceOf(1, 2, 1).firstDifferenceTo(sequenceOf(1, 2, 3)) } isEqualTo { Difference(2, 1, 3) }

        expectThat { sequenceOf(1, 2, 3).firstDifferenceTo(sequenceOf(1, 2)) } isEqualTo { LhsIsLonger(2) }
        expectThat { sequenceOf(1, 2).firstDifferenceTo(sequenceOf(1, 2, 3)) } isEqualTo { RhsIsLonger(2) }

        expectThat { sequenceOf(1, 2, 3).firstDifferenceTo(sequenceOf<Int>()) } isEqualTo { LhsIsLonger(0) }
        expectThat { sequenceOf<Int>().firstDifferenceTo(sequenceOf(1, 2, 3)) } isEqualTo { RhsIsLonger(0) }

        // sequence difference-checking is also great for working with other collections like e.g. maps
        expectThat {
            sortedMapOf(1 to "one", 2 to "two").asSequence()
                .map { it.key to it.value } // sorry, we have to map it to a pair, otherwise we would have to deal with special map stuff
                .firstDifferenceTo(sequenceOf(1 to "one", 2 to "two"))
        } isEqualTo { NoDifference() }
        expectThat {
            sortedMapOf(1 to "one", 2 to "oops").asSequence()
                .map { it.key to it.value }
                .firstDifferenceTo(sequenceOf(1 to "one", 2 to "two"))
        } isEqualTo { Difference(1, (2 to "oops"), (2 to "two")) }
    }
}