package iceland.utils

import org.junit.jupiter.api.Test as spec
import iceland.matchers.*

class ObjectSupportingArithmeticsSpecs {

    @spec
    fun `toString is propagated to the wrapped object's version`() {
        val someValue = 21
        val target = objectSupportingArithmetics(someValue)

        expectThat { target.toString() } isEqualTo { someValue.toString() }
    }

    @spec
    fun `equals is propagated to the wrapped object's version`() {
        val someValue = 21
        val target = objectSupportingArithmetics(someValue)

        // if the right hand side is an object supporting arithmetics as well, comparison
        // propagates it to the wrapped value and compares it against the extracted value of the right hand side
        expectThat { target == objectSupportingArithmetics(someValue) }.isTrue
        expectThat { target == objectSupportingArithmetics(99) }.isFalse
        expectThat { target == objectSupportingArithmetics(99.0) }.isFalse

        // otherwise equals propagates comparison to the wrapped value and compares against the plain rhs
        expectThat { target.equals(21) }.isTrue
        expectThat { target.equals(99) }.isFalse
        expectThat { target.equals(99.0) }.isFalse
    }

    @spec
    fun `hashCode is propagated to the wrapped object's version`() {
        val someValue = 753.0
        val target = objectSupportingArithmetics(someValue)

        expectThat { target.hashCode() } isEqualTo { someValue.hashCode() }
        expectThat { target.hashCode() } isNotEqualTo { (someValue + 1.0).hashCode() }
    }

    @spec
    fun `bytes are supported`() {
        // given
        val lhsAsAny : Any = 17.toByte()
        val rhsAsAny : Any = 4.toByte()
        val zeroAsAny : Any = 0.toByte()
        val negValueAsAny : Any = (-1).toByte()

        // when
        val lhs = objectSupportingArithmetics(lhsAsAny)
        val rhs = objectSupportingArithmetics(rhsAsAny)
        val zero = objectSupportingArithmetics(zeroAsAny)
        val negValue = objectSupportingArithmetics(negValueAsAny)

        // then
        expectThat { (+lhs).value } isEqualTo { 17.toByte() }
        expectThat { (-lhs).value } isEqualTo { (-17).toByte() }
        expectThat { (lhs.abs).value } isEqualTo { 17.toByte() }
        expectThat { ((-lhs).abs).value } isEqualTo { 17.toByte() }
        expectThat { (lhs + rhs).value } isEqualTo { 21.toByte() }
        expectThat { (lhs - rhs).value } isEqualTo { 13.toByte() }
        expectThat { (lhs * rhs).value } isEqualTo { 68.toByte() }
        expectThat { (lhs / rhs).value } isEqualTo { 4.toByte() }
        expectThat { (lhs % rhs).value } isEqualTo { 1.toByte() }

        expectThat { lhs > rhs }.isTrue
        expectThat { lhs >= rhs }.isTrue
        expectThat { lhs >= lhs }.isTrue
        expectThat { lhs < rhs }.isFalse
        expectThat { lhs <= rhs }.isFalse
        expectThat { lhs <= lhs }.isTrue

        expectThat { lhs.isPositive }.isTrue
        expectThat { zero.isPositive }.isTrue
        expectThat { negValue.isPositive }.isFalse

        expectThat { lhs.isGreaterThanZero }.isTrue
        expectThat { zero.isGreaterThanZero }.isFalse
        expectThat { negValue.isGreaterThanZero }.isFalse

        expectThat { negValue.isNegative }.isTrue
        expectThat { zero.isNegative }.isFalse
        expectThat { lhs.isNegative }.isFalse

        expectThat { negValue.isZero }.isFalse
        expectThat { zero.isZero }.isTrue
        expectThat { lhs.isZero }.isFalse
    }

    @spec
    fun `shorts are supported`() {
        // given
        val lhsAsAny : Any = 17.toShort()
        val rhsAsAny : Any = 4.toShort()
        val zeroAsAny : Any = 0.toShort()
        val negValueAsAny : Any = (-1).toShort()

        // when
        val lhs = objectSupportingArithmetics(lhsAsAny)
        val rhs = objectSupportingArithmetics(rhsAsAny)
        val zero = objectSupportingArithmetics(zeroAsAny)
        val negValue = objectSupportingArithmetics(negValueAsAny)

        // then
        expectThat { (+lhs).value } isEqualTo { 17.toShort() }
        expectThat { (-lhs).value } isEqualTo { (-17).toShort() }
        expectThat { (lhs.abs).value } isEqualTo { 17.toShort() }
        expectThat { ((-lhs).abs).value } isEqualTo { 17.toShort() }
        expectThat { (lhs + rhs).value } isEqualTo { 21.toShort() }
        expectThat { (lhs - rhs).value } isEqualTo { 13.toShort() }
        expectThat { (lhs * rhs).value } isEqualTo { 68.toShort() }
        expectThat { (lhs / rhs).value } isEqualTo { 4.toShort() }
        expectThat { (lhs % rhs).value } isEqualTo { 1.toShort() }

        expectThat { lhs > rhs }.isTrue
        expectThat { lhs >= rhs }.isTrue
        expectThat { lhs >= lhs }.isTrue
        expectThat { lhs < rhs }.isFalse
        expectThat { lhs <= rhs }.isFalse
        expectThat { lhs <= lhs }.isTrue

        expectThat { lhs.isPositive }.isTrue
        expectThat { zero.isPositive }.isTrue
        expectThat { negValue.isPositive }.isFalse

        expectThat { lhs.isGreaterThanZero }.isTrue
        expectThat { zero.isGreaterThanZero }.isFalse
        expectThat { negValue.isGreaterThanZero }.isFalse

        expectThat { negValue.isNegative }.isTrue
        expectThat { zero.isNegative }.isFalse
        expectThat { lhs.isNegative }.isFalse

        expectThat { negValue.isZero }.isFalse
        expectThat { zero.isZero }.isTrue
        expectThat { lhs.isZero }.isFalse
    }

    @spec
    fun `ints are supported`() {
        // given
        val lhsAsAny : Any = 17
        val rhsAsAny : Any = 4
        val zeroAsAny : Any = 0
        val negValueAsAny : Any = (-1)

        // when
        val lhs = objectSupportingArithmetics(lhsAsAny)
        val rhs = objectSupportingArithmetics(rhsAsAny)
        val zero = objectSupportingArithmetics(zeroAsAny)
        val negValue = objectSupportingArithmetics(negValueAsAny)

        // then
        expectThat { (+lhs).value } isEqualTo { 17 }
        expectThat { (-lhs).value } isEqualTo { (-17) }
        expectThat { (lhs.abs).value } isEqualTo { 17 }
        expectThat { ((-lhs).abs).value } isEqualTo { 17 }
        expectThat { (lhs + rhs).value } isEqualTo { 21 }
        expectThat { (lhs - rhs).value } isEqualTo { 13 }
        expectThat { (lhs * rhs).value } isEqualTo { 68 }
        expectThat { (lhs / rhs).value } isEqualTo { 4 }
        expectThat { (lhs % rhs).value } isEqualTo { 1 }

        expectThat { lhs > rhs }.isTrue
        expectThat { lhs >= rhs }.isTrue
        expectThat { lhs >= lhs }.isTrue
        expectThat { lhs < rhs }.isFalse
        expectThat { lhs <= rhs }.isFalse
        expectThat { lhs <= lhs }.isTrue

        expectThat { lhs.isPositive }.isTrue
        expectThat { zero.isPositive }.isTrue
        expectThat { negValue.isPositive }.isFalse

        expectThat { lhs.isGreaterThanZero }.isTrue
        expectThat { zero.isGreaterThanZero }.isFalse
        expectThat { negValue.isGreaterThanZero }.isFalse

        expectThat { negValue.isNegative }.isTrue
        expectThat { zero.isNegative }.isFalse
        expectThat { lhs.isNegative }.isFalse

        expectThat { negValue.isZero }.isFalse
        expectThat { zero.isZero }.isTrue
        expectThat { lhs.isZero }.isFalse
    }

    @spec
    fun `longs are supported`() {
        // given
        val lhsAsAny : Any = 17L
        val rhsAsAny : Any = 4L
        val zeroAsAny : Any = 0L
        val negValueAsAny : Any = -1L

        // when
        val lhs = objectSupportingArithmetics(lhsAsAny)
        val rhs = objectSupportingArithmetics(rhsAsAny)
        val zero = objectSupportingArithmetics(zeroAsAny)
        val negValue = objectSupportingArithmetics(negValueAsAny)

        // then
        expectThat { (+lhs).value } isEqualTo { 17L }
        expectThat { (-lhs).value } isEqualTo { -17L }
        expectThat { (lhs.abs).value } isEqualTo { 17L }
        expectThat { ((-lhs).abs).value } isEqualTo { 17L }
        expectThat { (lhs + rhs).value } isEqualTo { 21L }
        expectThat { (lhs - rhs).value } isEqualTo { 13L }
        expectThat { (lhs * rhs).value } isEqualTo { 68L }
        expectThat { (lhs / rhs).value } isEqualTo { 4L }
        expectThat { (lhs % rhs).value } isEqualTo { 1L }

        expectThat { lhs > rhs }.isTrue
        expectThat { lhs >= rhs }.isTrue
        expectThat { lhs >= lhs }.isTrue
        expectThat { lhs < rhs }.isFalse
        expectThat { lhs <= rhs }.isFalse
        expectThat { lhs <= lhs }.isTrue

        expectThat { lhs.isPositive }.isTrue
        expectThat { zero.isPositive }.isTrue
        expectThat { negValue.isPositive }.isFalse

        expectThat { lhs.isGreaterThanZero }.isTrue
        expectThat { zero.isGreaterThanZero }.isFalse
        expectThat { negValue.isGreaterThanZero }.isFalse

        expectThat { negValue.isNegative }.isTrue
        expectThat { zero.isNegative }.isFalse
        expectThat { lhs.isNegative }.isFalse

        expectThat { negValue.isZero }.isFalse
        expectThat { zero.isZero }.isTrue
        expectThat { lhs.isZero }.isFalse
    }

    @spec
    fun `floats are supported`() {
        // given
        val lhsAsAny : Any = 17.0f
        val rhsAsAny : Any = 4.0f
        val zeroAsAny : Any = 0.0f
        val negValueAsAny : Any = -1.0f

        // when
        val lhs = objectSupportingArithmetics(lhsAsAny)
        val rhs = objectSupportingArithmetics(rhsAsAny)
        val zero = objectSupportingArithmetics(zeroAsAny)
        val negValue = objectSupportingArithmetics(negValueAsAny)

        // then
        expectThat { (+lhs).value } isEqualTo { 17.0f }
        expectThat { (-lhs).value } isEqualTo { -17.0f }
        expectThat { (lhs.abs).value } isEqualTo { 17.0f }
        expectThat { ((-lhs).abs).value } isEqualTo { 17.0f }
        expectThat { (lhs + rhs).value } isEqualTo { 21.0f }
        expectThat { (lhs - rhs).value } isEqualTo { 13.0f }
        expectThat { (lhs * rhs).value } isEqualTo { 68.0f }
        expectThat { (lhs / rhs).value } isEqualTo { 4.25f }
        expectThat { (lhs % rhs).value } isEqualTo { 1.0f }

        expectThat { lhs > rhs }.isTrue
        expectThat { lhs >= rhs }.isTrue
        expectThat { lhs >= lhs }.isTrue
        expectThat { lhs < rhs }.isFalse
        expectThat { lhs <= rhs }.isFalse
        expectThat { lhs <= lhs }.isTrue

        expectThat { lhs.isPositive }.isTrue
        expectThat { zero.isPositive }.isTrue
        expectThat { negValue.isPositive }.isFalse

        expectThat { lhs.isGreaterThanZero }.isTrue
        expectThat { zero.isGreaterThanZero }.isFalse
        expectThat { negValue.isGreaterThanZero }.isFalse

        expectThat { negValue.isNegative }.isTrue
        expectThat { zero.isNegative }.isFalse
        expectThat { lhs.isNegative }.isFalse

        expectThat { negValue.isZero }.isFalse
        expectThat { zero.isZero }.isTrue
        expectThat { lhs.isZero }.isFalse
    }

    @spec
    fun `doubles are supported`() {
        // given
        val lhsAsAny : Any = 17.0
        val rhsAsAny : Any = 4.0
        val zeroAsAny : Any = 0.0
        val negValueAsAny : Any = -1.0

        // when
        val lhs = objectSupportingArithmetics(lhsAsAny)
        val rhs = objectSupportingArithmetics(rhsAsAny)
        val zero = objectSupportingArithmetics(zeroAsAny)
        val negValue = objectSupportingArithmetics(negValueAsAny)

        // then
        expectThat { (+lhs).value } isEqualTo { 17.0 }
        expectThat { (-lhs).value } isEqualTo { -17.0 }
        expectThat { (lhs.abs).value } isEqualTo { 17.0 }
        expectThat { ((-lhs).abs).value } isEqualTo { 17.0 }
        expectThat { (lhs + rhs).value } isEqualTo { 21.0 }
        expectThat { (lhs - rhs).value } isEqualTo { 13.0 }
        expectThat { (lhs * rhs).value } isEqualTo { 68.0 }
        expectThat { (lhs / rhs).value } isEqualTo { 4.25 }
        expectThat { (lhs % rhs).value } isEqualTo { 1.0 }

        expectThat { lhs > rhs }.isTrue
        expectThat { lhs >= rhs }.isTrue
        expectThat { lhs >= lhs }.isTrue
        expectThat { lhs < rhs }.isFalse
        expectThat { lhs <= rhs }.isFalse
        expectThat { lhs <= lhs }.isTrue

        expectThat { lhs.isPositive }.isTrue
        expectThat { zero.isPositive }.isTrue
        expectThat { negValue.isPositive }.isFalse

        expectThat { lhs.isGreaterThanZero }.isTrue
        expectThat { zero.isGreaterThanZero }.isFalse
        expectThat { negValue.isGreaterThanZero }.isFalse

        expectThat { negValue.isNegative }.isTrue
        expectThat { zero.isNegative }.isFalse
        expectThat { lhs.isNegative }.isFalse

        expectThat { negValue.isZero }.isFalse
        expectThat { zero.isZero }.isTrue
        expectThat { lhs.isZero }.isFalse
    }
    
}