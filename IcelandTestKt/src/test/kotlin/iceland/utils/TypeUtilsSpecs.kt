package iceland.utils

import iceland.exceptions.InvalidOperationException
import iceland.matchers.*
import org.junit.jupiter.api.Test as spec

class TypeUtilsSpecs {

    @spec
    fun `we can ask, whether a given object is numeric`() {
        expectThat { 12.toByte().isNumeric }.isTrue
        expectThat { 12.toUByte().isNumeric }.isTrue
        expectThat { 12.toShort().isNumeric }.isTrue
        expectThat { 12.toUShort().isNumeric }.isTrue
        expectThat { 12.isNumeric }.isTrue
        expectThat { 12.isNumeric }.isTrue
        expectThat { 12L.isNumeric }.isTrue
        expectThat { 12uL.isNumeric }.isTrue
        expectThat { (12.0).isNumeric }.isTrue
        expectThat { (12.0f).isNumeric }.isTrue

        expectThat { "just a string".isNumeric }.isFalse
        expectThat { null.isNumeric }.isFalse

        // important: we explicitly do NOT support BigInteger and BigDecimal here at the moment
        // for the sake of compatibility to JavaScript
        // ... this will be subject to some discussions and is very likely to change in future versions
    }

    @spec
    fun `we can ask, whether a given object is integral`() {
        expectThat { 12.toByte().isIntegral }.isTrue
        expectThat { 12.toUByte().isIntegral }.isTrue
        expectThat { 12.toShort().isIntegral }.isTrue
        expectThat { 12.toUShort().isIntegral }.isTrue
        expectThat { 12.isIntegral }.isTrue
        expectThat { 12.isIntegral }.isTrue
        expectThat { 12L.isIntegral }.isTrue
        expectThat { 12uL.isIntegral }.isTrue

        expectThat { (12.0).isIntegral }.isFalse
        expectThat { "just a string".isIntegral }.isFalse
        expectThat { null.isNumeric }.isFalse

        // important: we explicitly do NOT support BigInteger and BigDecimal here at the moment
        // for the sake of compatibility to JavaScript
        // ... this will be subject to some discussions and is very likely to change in future versions
    }

    @spec
    fun `we can ask, whether a given object is a floating-point number`() {
        expectThat { (12.0).isFloatingPoint }.isTrue
        expectThat { (12.0f).isFloatingPoint }.isTrue

        expectThat { 12.isFloatingPoint }.isFalse
        expectThat { "just a string".isFloatingPoint }.isFalse
        expectThat { null.isFloatingPoint }.isFalse

        // important: we explicitly do NOT support BigDecimal here at the moment
        // for the sake of compatibility to JavaScript
        // ... this will be subject to some discussions and is very likely to change in future versions
    }

    @spec
    fun `we can obtain a Double from any numeric type`() {
        expectThat { (12.0).asDouble } isEqualTo { 12.0 }
        expectThat { (12.0f).asDouble } isEqualTo { 12.0 }
        expectThat { 12.asDouble } isEqualTo { 12.0 }
        expectThat { 12.toUByte().asDouble } isEqualTo { 12.0 }

        expectThat { "just a string".asDouble } throws { it is InvalidOperationException }
    }
}